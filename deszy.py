def szyfr_cezara(wiadomosc, klucz):

    zaszyfrowana_wiadomosc = ''


    for x  in wiadomosc:
        if x.isalpha():
            liczba = ord(x)
            liczba -= klucz

            if x.isupper():
                if liczba > 90:
                    liczba -= 26
                elif liczba < 65:
                    liczba += 26
            else:
                if liczba > 122:
                    liczba -=26
                elif liczba < 97:
                    liczba += 26

            zaszyfrowana_wiadomosc += chr(liczba)
        else:
             zaszyfrowana_wiadomosc += x
    return zaszyfrowana_wiadomosc

print('Podaj swoja wiadomosc do odszyfrowania')
wiadomosc = input()

print('Podaj klucz wiadomosci zaszyfrowanej')
klucz = int(input())

print('Twojaodszyfrowana wiadomosc to')
print(szyfr_cezara(wiadomosc, klucz))